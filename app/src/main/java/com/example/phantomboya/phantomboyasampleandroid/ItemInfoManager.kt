package com.example.phantomboya.phantomboyasampleandroid

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.gson.Gson


class ItemInfoManager(private val activity : Activity) {
    var itemInfoListShdPrfs:SharedPreferences = activity.getSharedPreferences("item_info_list",Context.MODE_PRIVATE)
    val gson = Gson()

    public fun getItemInfoListFromShdPrfs() :ItemList{
        val itemList = gson.fromJson<ItemList>(this.itemInfoListShdPrfs.getString("itemInfoList",""),ItemList::class.java)
        return itemList
    }

    public fun putItemInfolistToShdPrfs(itemList: ItemList){
        val editItemInfolistShdPrfs = this.itemInfoListShdPrfs.edit()
        editItemInfolistShdPrfs.putString("itemInfoList",gson.toJson(itemList)).commit()
    }

    public fun setFlgisDeleted(position: Int,itemList: ItemList){
        itemList.itemInfoList[position].isDeleted = true
    }

    public fun deleteItem(itemList: ItemList){
        val tempArray=ArrayList<ItemInfo>()

        for(i in itemList.itemInfoList.indices) {
            if (itemList.itemInfoList[i].isDeleted) {

            } else {
                tempArray.add(itemList.itemInfoList[i])
            }
        }

        itemList.itemInfoList.clear()
        itemList.itemInfoList.addAll(tempArray)
        putItemInfolistToShdPrfs(itemList)

    }



}