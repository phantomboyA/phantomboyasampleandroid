package com.example.phantomboya.phantomboyasampleandroid

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.example.phantomboya.phantomboyasampleandroid.R.id.delete_button
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.list_item.*
import kotlinx.android.synthetic.main.list_item.view.*
import java.lang.reflect.Type

/**
 * Created by phantomboyA on 2018/03/03.
 */
class MyRecyclerAdapter(private val activity : Activity):RecyclerView.Adapter<MyRecyclerViewHolder>() {

    //nullable !! ? をやめる方法
    private val itemInfoListShdPrfs: SharedPreferences = activity.getSharedPreferences("item_info_list",Context.MODE_PRIVATE)
    private val itemInfoMgr:ItemInfoManager by lazy { ItemInfoManager(activity) }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyRecyclerViewHolder {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.list_item, parent,false)
        //xmlファイルからviewを作るのがinflater
        return MyRecyclerViewHolder(view)

    }

    override fun getItemCount(): Int {
        var num=0

        if(itemInfoListShdPrfs.contains("itemInfoList")){
            val itemList = itemInfoMgr.getItemInfoListFromShdPrfs()
            num = itemList.itemInfoList.size
        }else{
            num = 0
        }
        return num
    }

    override fun onBindViewHolder(holder: MyRecyclerViewHolder?, position: Int) {

        //itemInfoListShdPrfs!!.edit().clear().commit()

        var itemList = itemInfoMgr.getItemInfoListFromShdPrfs()

        holder!!.v.findViewById<TextView>(R.id.item_name).text =itemList.itemInfoList[position].itemName.toString()
        holder.v.findViewById<TextView>(R.id.item_number).text = itemList.itemInfoList[position].itemCount.toString()

        holder.v.setOnClickListener {i ->
            //Intentのインスタンスを作成
            val intent= Intent(activity,MainActivity::class.java)
            intent.putExtra("posi",position.toString())
            activity.startActivity(intent)

        }

        holder.v.delete_button.setOnClickListener{
            // ダイアログを作成して表示
            AlertDialog.Builder(activity).apply {
                setTitle("アイテム削除")
                var toBeDeletedItemName=itemList.itemInfoList[position].itemName
                setMessage(toBeDeletedItemName+"を削除しますか？")
                setPositiveButton("OK", DialogInterface.OnClickListener { _, _ ->
                    // OKをタップしたときの処理
                    //選択されたアイテムの削除フラグをtrueにする
                    itemInfoMgr.setFlgisDeleted(position,itemList)
                    //削除フラグがtrueのアイテムを配列から外す
                    itemInfoMgr.deleteItem(itemList)
                    Toast.makeText(context, toBeDeletedItemName+"を削除しました。", Toast.LENGTH_LONG).show()
                    activity.recycler_view.adapter.notifyDataSetChanged()
                })
                setNegativeButton("Cancel", null)
                show()
            }

        }

    }


}

class MyRecyclerViewHolder(val v: View):RecyclerView.ViewHolder(v){

}