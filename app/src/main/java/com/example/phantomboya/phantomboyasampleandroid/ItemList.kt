package com.example.phantomboya.phantomboyasampleandroid

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson

class ItemList(var listName:String) {
    var itemInfoList  = ArrayList<ItemInfo>()
    private lateinit var itemInfoListShdPrfs : SharedPreferences

    public fun putItemInfolistToShdPrfs(itemList: ItemList){
        val gson: Gson = Gson()
        val editItemInfolistShdPrfs = this.itemInfoListShdPrfs.edit()
        editItemInfolistShdPrfs.putString("itemInfoList",gson.toJson(itemList)).commit()
    }


}