package com.example.phantomboya.phantomboyasampleandroid

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.list_item.*

class ListActivity : AppCompatActivity() {

    private val itemInfoMgr:ItemInfoManager by lazy { ItemInfoManager(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = MyRecyclerAdapter(this)

        create_new_item.setOnClickListener { v ->
            val intent= Intent(this,MainActivity::class.java)

            //itemInfoインスタンスの生成
            var itemList = ItemList("itemList")
            var itemNum = recycler_view.adapter.itemCount

            val newItemInfo = ItemInfo(itemNum.toString(),"新しいアイテムだーよ",0,false)

            if(itemNum == 0){
                itemList.itemInfoList = ArrayList<ItemInfo>()
            }else{
                itemList=itemInfoMgr.getItemInfoListFromShdPrfs()
            }

            itemList.itemInfoList.add(newItemInfo)

            //shdprfsに保存
            itemInfoMgr.putItemInfolistToShdPrfs(itemList)

            //遷移先値渡し処理
            intent.putExtra("posi",itemNum) //posiというインテントにitemNumの値を代入

            startActivity(intent)
        }

    }

    fun init(){

    }

    override fun onResume() {
        super.onResume()
        recycler_view.adapter.notifyDataSetChanged()

    }







}
